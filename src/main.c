#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "aes/aes.h"

//256 bit key
const uint8_t aes_key[] = {
    0xff, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
    0xff, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
};

static uint8_t * decrypt_AES(const uint8_t * inbuf, uint32_t len)
{
    uint8_t * output = NULL;

    // calculate proper output len
    // buffer should be multiple by 16, after decoding remain bytes will be filled with 0x00
    uint32_t rest = len % 16;
    output = calloc(1, (len + rest));

    if (output == NULL)
    {
        printf("Cant allocate memory [%d]\n", __LINE__);
        return NULL;
    }

    // decode part by part
    for (uint32_t i = 0; i < len; i += 16)
    {
        uint8_t buf[16] = { 0 };
        AES_ECB_decrypt(inbuf, aes_key, &buf[0], 16);
        memcpy(output, buf, 16);
        output += 16;
        inbuf += 16;
    }

    //get pointer at the beginning of "output" buffer
    output -= (len + rest);

    return output;
}

int main(void)
{
	// encrypted message, 256 bit ECB mode
    const uint8_t input_str[] = {0xBB, 0x50, 0x5D, 0x7E, 0xCC, 0x9F, 0xFE, 0xC1,
                                 0x89, 0x5E, 0xD2, 0x86, 0x3A, 0x35, 0xDC, 0x12,
                                 0x47, 0x04, 0x7D, 0xBC, 0xB7, 0x9B, 0xCC, 0xBD,
                                 0x10, 0x89, 0xE9, 0xA9, 0xE0, 0xAE, 0x1E, 0x1F };

    uint32_t len = sizeof(input_str);

    uint8_t* out_str = decrypt_AES(input_str, len);

    //should be "this is test string in text"
    if (out_str == NULL)
    {
        printf("Decryption fails \n");
		return -1;
    }
	
	printf((char *)out_str);

    return 0;
}
