# AES decryptor

This is simple AES decryptor.

Default example:

- key 256 byt
- ECB mode

# Compile

Project created in Visual Studio, but can be compiled with gcc:

```
gcc -Wall -c main.c
gcc -Wall -c aes/aes.c
gcc -o config_check main.o aes.o
```